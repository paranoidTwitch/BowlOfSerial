﻿using SerialTester.SerialSupport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SerialTester.Controls
{
    /// <summary>
    /// Interaction logic for IOControl.xaml
    /// </summary>
    public partial class SerialIOControl : UserControl
    {
        private Regex _hexFinderRegex = new Regex("[0-9A-Fa-f -]+", RegexOptions.Compiled);

        private bool _rawModeEnabledRx = false, _rawModeEnabledTx = false;
        public bool RawModeEnabledRx
        {
            get
            {
                return _rawModeEnabledRx;
            }
            set
            {
                _rawModeEnabledRx = value;
                if (!rawModeCheckBoxRx.Dispatcher.CheckAccess())
                {
                    rawModeCheckBoxRx.Dispatcher.Invoke(() =>
                    {
                        rawModeCheckBoxRx.IsChecked = value;
                    });
                }
            }
        }

        public bool RawModeEnabledTx
        {
            get
            {
                return _rawModeEnabledTx;
            }
            set
            {
                _rawModeEnabledTx = value;
                if (!rawModeCheckBoxTx.Dispatcher.CheckAccess())
                {
                    rawModeCheckBoxTx.Dispatcher.Invoke(() =>
                    {
                        rawModeCheckBoxTx.IsChecked = value;
                    });
                }
            }
        }

        public SerialIOControl()
        {
            InitializeComponent();
            SerialController.DataIsAvailable += SerialController_DataIsAvailable;
            SerialController.ConnectionedOpened += SerialController_ConnectionedOpened;
            SerialController.ConnectionedClosed += SerialController_ConnectionedClosed;
        }
        
        void SerialController_ConnectionedOpened()
        {
            if (!sendButton.Dispatcher.CheckAccess())
            {
                sendButton.Dispatcher.Invoke(SerialController_ConnectionedOpened);
                return;
            }

            sendButton.IsEnabled = true;
            console.Text = "";
        }

        void SerialController_ConnectionedClosed()
        {
            if (!sendButton.Dispatcher.CheckAccess())
            {
                sendButton.Dispatcher.Invoke(SerialController_ConnectionedClosed);
                return;
            }

            sendButton.IsEnabled = false;

            console.Text = String.Concat(console.Text, Environment.NewLine);
            console.Inlines.Add(new Run()
            {
                Foreground = new SolidColorBrush()
                {
                    Color = Colors.Red
                },
                Text = "Connection Closed."
            });
        }

        void SerialController_DataIsAvailable(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            byte[] incomingBuffer;
            SerialController.ReadBytes(out incomingBuffer);

            if (RawModeEnabledRx)
            {
                console.Dispatcher.Invoke(() =>
                {
                    console.Text = String.Concat(console.Text, ByteArrayToString(incomingBuffer));
                });
            }
            else
            {
                console.Dispatcher.Invoke(() =>
                {
                    console.Text = String.Concat(console.Text, SerialController.CurrentSerialSettings.CurrentEncoding.GetString(incomingBuffer));
                });
            }
        }

        private void sendButton_Click(object sender, RoutedEventArgs e)
        {
            byte[] bytesToSend;
            if (RawModeEnabledTx)
            {
                string inputString = inputTextBox.Text.Replace(" ", "").Replace("-", "");

                if (inputString.Length % 2 == 0)
                {

                    bytesToSend = StringToByteArray(inputString);
                }
                else
                {
                    MessageBox.Show("Raw strings must have an even number of characters, try adding a leading zero.");
                    return;
                }
            }
            else
            {
                bytesToSend = SerialController.CurrentSerialSettings.CurrentEncoding.GetBytes(inputTextBox.Text);
            }

            SerialController.SendBytes(bytesToSend);
            inputTextBox.Text = "";
            inputTextBox.Focus();
        }

        public static byte[] StringToByteArray(string hex)
        {   
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        public static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2} ", b);
            return hex.ToString();
        }

        private void inputTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (RawModeEnabledTx)
            {
                foreach (TextChange change in e.Changes)
                {
                    if (change.AddedLength > 0)
                    {
                        TextBox textBox = sender as TextBox;
                        TextChange textChange = change;
                        int iAddedLength = textChange.AddedLength;
                        int iOffset = textChange.Offset;

                        string addedString = textBox.Text.Substring(iOffset, iAddedLength);

                        if (_hexFinderRegex.Match(addedString).Length != addedString.Length)
                        {
                            textBox.Text = textBox.Text.Remove(iOffset, iAddedLength);
                            textBox.CaretIndex = textBox.Text.Length;
                        }
                    }
                }
            }
        }

        private void rawModeCheckBox_CheckChanged(object sender, RoutedEventArgs e)
        {
            _rawModeEnabledRx = rawModeCheckBoxRx.IsChecked.HasValue ? rawModeCheckBoxRx.IsChecked.Value : false;
            _rawModeEnabledTx = rawModeCheckBoxTx.IsChecked.HasValue ? rawModeCheckBoxTx.IsChecked.Value : false;
            inputTextBox.Text = "";
        }

        private void inputTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                sendButton_Click(sendButton, null);
            }
        }
    }
}
