﻿using SerialTester.SerialSupport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SerialTester.Controls
{
    /// <summary>
    /// Interaction logic for SerialConfigControl.xaml
    /// </summary>
    public partial class SerialConfigToolbar : UserControl
    {
        public SerialConfigToolbar()
        {
            InitializeComponent();
            SerialController.DeviceListRefreshed += SerialController_DeviceListRefreshed;
            SerialController.ConnectionedOpened += SerialController_ConnectionedOpened;
            SerialController.ConnectionedClosed += SerialController_ConnectionedClosed;
        }

        void SerialController_ConnectionedOpened()
        {
            if (!connectButton.Dispatcher.CheckAccess())
            {
                connectButton.Dispatcher.Invoke(SerialController_ConnectionedClosed);
                return;
            }

            connectButton.IsEnabled = false;
            connectButton.Content = this.Resources["RunDisabled"];

            DisconnectButton.IsEnabled = true;
            DisconnectButton.Content = this.Resources["Stop"];

            SettingsButton.IsEnabled = false;
            SettingsButton.Content = this.Resources["GearDisabled"];

            RefreshButton.IsEnabled = false;
            RefreshButton.Content = this.Resources["RefreshDisabled"];

            commSelectComboBox.IsEnabled = false;
        }

        void SerialController_ConnectionedClosed()
        {
            if (!connectButton.Dispatcher.CheckAccess())
            {
                connectButton.Dispatcher.Invoke(SerialController_ConnectionedClosed);
                return;
            }

            connectButton.IsEnabled = true;
            connectButton.Content = this.Resources["Run"];

            DisconnectButton.IsEnabled = false;
            DisconnectButton.Content = this.Resources["StopDisabled"];

            SettingsButton.IsEnabled = true;
            SettingsButton.Content = this.Resources["Gear"];

            RefreshButton.IsEnabled = true;
            RefreshButton.Content = this.Resources["Refresh"];

            commSelectComboBox.IsEnabled = true;
        }

        void SerialController_DeviceListRefreshed()
        {
            commSelectComboBox.Items.Clear();
            foreach (string deviceName in SerialController.CurrentDeviceList)
            {
                commSelectComboBox.Items.Add(deviceName);
            }

            if (commSelectComboBox.Items.Count > 0)
            {
                commSelectComboBox.SelectedIndex = 0;
            }
        }

        private void ConnectButton_Click(object sender, RoutedEventArgs e)
        {
            SerialController.OpenConnection(commSelectComboBox.SelectedItem as string);
        }

        private void DisconnectButton_Click(object sender, RoutedEventArgs e)
        {
            SerialController.CloseConnection();
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            SerialController.RefreshConnectedDevices();
        }

        private void SettingsButton_Click(object sender, RoutedEventArgs e)
        {
            SettingsPopup.IsOpen = !SettingsPopup.IsOpen;
        }
    }
}
