﻿using SerialTester.SerialSupport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SerialTester.Controls
{
    /// <summary>
    /// Interaction logic for SerialEncodingControl.xaml
    /// </summary>
    public partial class SerialEncodingControl : UserControl
    {
        public SerialEncodingControl()
        {
            InitializeComponent();

            foreach (var item in Encoding.GetEncodings())
            {
                textEncodingComboBox.Items.Add(item.Name);
            }

            textEncodingComboBox.SelectedItem = SerialSettings.DefaultValues.CurrentEncoding.WebName;
            textEncodingComboBox.SelectionChanged += textEncodingComboBox_SelectionChanged;
        }

        private void textEncodingComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                string value = (string)textEncodingComboBox.SelectedItem;
                SerialController.CurrentSerialSettings.CurrentEncoding = Encoding.GetEncoding(textEncodingComboBox.SelectedItem as string);
            }
            catch
            {
                MessageBox.Show("Sorry there was a problem setting your text encoding.");
                textEncodingComboBox.SelectedItem = SerialController.CurrentSerialSettings.CurrentEncoding.EncodingName;
            }
        }
    }
}
