﻿using SerialTester.SerialSupport;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SerialTester.Controls
{
    /// <summary>
    /// Interaction logic for SerialSettingsControl.xaml
    /// </summary>
    public partial class SerialSettingsControl : UserControl
    {
        public SerialSettingsControl()
        {
            InitializeComponent();
            SetupComboBoxes();
            SetDefaultValues();
            WireEvents();
        }

        private void SetupComboBoxes()
        {
            foreach (int reqBaudRate in SerialSettings.RecomendedBaudRates)
            {
                baudRateComboBox.Items.Add(reqBaudRate);
            }

            for (int i = 1; i <= 12; i++)
            {
                dataBitsComboBox.Items.Add(i);
            }

            foreach (var item in Enum.GetValues(typeof(Parity)))
            {
                parityComboBox.Items.Add(item);
            }

            foreach (var item in Enum.GetValues(typeof(StopBits)))
            {
                stopBitsComboBox.Items.Add(item);
            }

            
        }

        private void SetDefaultValues()
        {
            
            baudRateComboBox.SelectedItem = SerialSettings.DefaultValues.BaudRate;
            dataBitsComboBox.SelectedItem = SerialSettings.DefaultValues.DataBits;
            parityComboBox.SelectedItem = SerialSettings.DefaultValues.ParitySetting;
            stopBitsComboBox.SelectedItem = SerialSettings.DefaultValues.StopBitsSetting;
            
        }

        private void WireEvents()
        {
            baudRateComboBox.SelectionChanged += baudRateComboBox_SelectionChanged;
            dataBitsComboBox.SelectionChanged += dataBitsComboBox_SelectionChanged;
            parityComboBox.SelectionChanged += parityComboBox_SelectionChanged;
            stopBitsComboBox.SelectionChanged += stopBitsComboBox_SelectionChanged;
        }

        private void baudRateComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                SerialController.CurrentSerialSettings.BaudRate = Convert.ToInt32(baudRateComboBox.SelectedItem);
            }
            catch
            {
                MessageBox.Show("Sorry there was a problem setting your baud rate.");
                baudRateComboBox.SelectedItem = SerialController.CurrentSerialSettings.BaudRate;
            }
        }

        private void dataBitsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                SerialController.CurrentSerialSettings.DataBits = Convert.ToInt32(dataBitsComboBox.SelectedItem);
            }
            catch
            {
                MessageBox.Show("Sorry there was a problem setting your data bits.");
                dataBitsComboBox.SelectedItem = SerialController.CurrentSerialSettings.DataBits;
            }
        }

        private void parityComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                SerialController.CurrentSerialSettings.ParitySetting = (Parity)parityComboBox.SelectedItem;
            }
            catch
            {
                MessageBox.Show("Sorry there was a problem setting your parity.");
                parityComboBox.SelectedItem = SerialController.CurrentSerialSettings.ParitySetting;
            }
        }

        private void stopBitsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                SerialController.CurrentSerialSettings.StopBitsSetting = (StopBits)stopBitsComboBox.SelectedItem;
            }
            catch
            {
                MessageBox.Show("Sorry there was a problem setting your stop bits.");
                stopBitsComboBox.SelectedItem = SerialController.CurrentSerialSettings.StopBitsSetting;
            }
        }
    }
}
