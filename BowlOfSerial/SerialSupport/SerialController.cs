﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SerialTester.SerialSupport
{
    public static class SerialController
    {
        public delegate void DeviceListRefreshDelegate();
        public static event DeviceListRefreshDelegate DeviceListRefreshed;

        public delegate void DataAvailableDelegate(object sender, SerialDataReceivedEventArgs e);
        public static event DataAvailableDelegate DataIsAvailable;

        public delegate void ConnectionOpenedDelegate();
        public static event ConnectionOpenedDelegate ConnectionedOpened;

        public delegate void ConnectionClosedDelegate();
        public static event ConnectionClosedDelegate ConnectionedClosed;

        private static SerialPort _currentSerialPort;

        private static string[] _currentDeviceList;
        public static string[] CurrentDeviceList { get { return _currentDeviceList; } }

        public static SerialSettings CurrentSerialSettings { get; set; }

        private static Task _watchDogTask;
        private static CancellationTokenSource _cancellTokenSource;

        static SerialController()
        {
            CurrentSerialSettings = new SerialSettings();
            _cancellTokenSource = new CancellationTokenSource();
        }

        public static bool IsConnected
        {
            get
            {
                if (_currentSerialPort == null)
                {
                    return false;
                }
                else
                {
                    return _currentSerialPort.IsOpen;
                }
            }
        }

        public static void RefreshConnectedDevices()
        {
            _currentDeviceList = SerialPort.GetPortNames();
            if (DeviceListRefreshed != null)
            {
                DeviceListRefreshed();
            }
        }

        public static void OpenConnection(string portName)
        {
            _currentSerialPort = new SerialPort(portName, CurrentSerialSettings.BaudRate, CurrentSerialSettings.ParitySetting, CurrentSerialSettings.DataBits, CurrentSerialSettings.StopBitsSetting);
            _currentSerialPort.Open();
            _currentSerialPort.DataReceived += _currentSerialPort_DataReceived;

            _watchDogTask = Task.Factory.StartNew(() =>
            {
                CancellationToken token = _cancellTokenSource.Token;

                while (IsConnected && !token.IsCancellationRequested)
                {
                    Task.Delay(100, token).Wait(token);
                }

                if (!token.IsCancellationRequested)
                {
                    CloseConnection();
                }
            });

            if (ConnectionedOpened != null)
            {
                ConnectionedOpened();
            }
        }

        public static void CloseConnection()
        {
            _currentSerialPort.DataReceived -= _currentSerialPort_DataReceived;

            if (IsConnected)
            {
                _currentSerialPort.Close();
            }

            if (!_watchDogTask.IsCompleted)
            {
                _cancellTokenSource.Cancel();
            }

            if (ConnectionedClosed != null)
            {
                ConnectionedClosed();
            }

        }

        public static void SendBytes(byte[] bytesToSend)
        {
            if (IsConnected)
            {
                _currentSerialPort.Write(bytesToSend, 0, bytesToSend.Length);
            }
            else
            {
                throw new InvalidOperationException("Please open the connection first.");
            }
        }

        public static void ReadBytes(out byte[] destinationArray)
        {
            if (IsConnected)
            {
                int bytesToRead = _currentSerialPort.BytesToRead;
                destinationArray = new byte[bytesToRead];
                _currentSerialPort.Read(destinationArray, 0, bytesToRead);
            }
            else
            {
                throw new InvalidOperationException("Please open the connection first.");
            }
        }

        private static void _currentSerialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            if (DataIsAvailable != null)
            {
                DataIsAvailable(sender, e);
            }
        }
    }
}
