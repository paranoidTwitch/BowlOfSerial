﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerialTester.SerialSupport
{
    public class SerialSettings
    {
        public static readonly int[] RecomendedBaudRates = new int[] { 300, 600, 1200, 2400, 4800, 9600, 14400, 19200, 28800, 38400, 57600, 115200 };

        public static readonly SerialSettings DefaultValues = new SerialSettings();

        public int BaudRate { get; set; }
        public Parity ParitySetting { get; set; }
        public int DataBits { get; set; }
        public StopBits StopBitsSetting { get; set; }
        public Encoding CurrentEncoding { get; set; }

        public SerialSettings()
        {
            BaudRate = 9600;
            ParitySetting = Parity.None;
            DataBits = 8;
            StopBitsSetting = StopBits.One;
            CurrentEncoding = Encoding.Default;
        }
    }
}
