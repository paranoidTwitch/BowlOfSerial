# BowlOfSerial
A dead simple .NET Serial Testing Client. Great for Arduino.

![A basic view](/ReadmeMedia/Screenshot.png)

The controls should be pretty straight forward. Select your port from the drop down. Hit refresh if you have connected your device after starting the program, or use that to clear devices no longer attached should they stay in the menu for some reason. Click the gear to set you serial settings like baud rate, data bits, etc. When you are ready hit the play button to connect. When you are finished click the stop button to disconnect.

The raw check box at the bottom allows you send explict byte values use one of the following formats.

```
00ff0102
00-ff-01-02
00 ff 01 02
```

When in raw mode the console will display the raw values from your device.

When the raw checkbox is not check all input/output will be encoded using the currecnt encoding selected in the dropdown to the right of the raw checkbox.
